# Markdown to Confluence Gitlab CI Template

Detect changed markdown files and using yaml front matter, munge, and upload to Confluence.

This functions as a piecemeal state machine, cobbled together with many parts; but it forks and triggers child pipelines for each updated page, so it should be able to handle many in parallel. It could have its own image, but it doesn't, so... Anyway, it works, and doesn't pretend to do much. The concept is manage a set of pages with optional images in Gitlab, then push these to an existing page in Confluence when there are updates, and if necessary upload attachments. That is all.

Doing this is generally a bad idea, since there should only be one source of truth, but if you can justify having this page available in Confluence this can help. However to that end, this add a warning at the top of the page that the originating source is elsewhere, and links to the repository.

## Limitations

First and foremost, this has to do with Confluence... so I think we all know that going to be the biggest limitation.

- Pandoc's Jira writer doesn't produce very pretty results.
  - They are the best in many cases.
  - Syntax highlighting is null, but still in a code block.
- Pandoc's HTML writer doesn't produce very pretty results.
  - Well, it actually does, but in standalone mode, I was having errors.
  - Plain HTML works, just is substandard to wiki markup in this case.
- Atlassian's support for markdown is woefull, to put it mildly.
  - Year's of customer seeking better markdown support, and still abyssmal.
  - You like tables? Too bad; no soup for you!
  - But some things work. so ¯\\\_(ツ)\_/¯
- Currently uploading associated pictures on every markdown update.
  - Intend to diff against attachments to avoid this.

## Steps

1. Get list of markdown files modified.
2. For each, get list of images.
3. For each markdown file with `confluence.id`:
   1. For each image:
      1. Download attachment.
      2. Diff attachment with image.
      3. If required, upload attachment.
   2. Add caution at top noting the source.
   3. Convert markdown.
   4. Upload the file.

*note: That part about downloading and diffing the attachments is just straight-up a lie at the moment.*

## Requirements

- Confluence API credentials
- Existing page in Confluence
- YAML front matter (described below)

This makes no effort to create pages on your behalf. Even though it could easily be expanded to use `ancestor_id` in the front matter with the scan api to create and manage whole trees of pages... for now we execute the [KISS principle](https://en.wikipedia.org/wiki/KISS_principle).

### YAML Front Matter

```yaml
title: Test Post Please Ignore
confluence:
    id: "306122808"
    space: ABCD
```

In markdown, that would be at the top of the file as:

```
---
title: Test Post Please Ignore
confluence:
    id: "306122808"
    space: ABCD
---

# File Starts Here
```

- `title`: Title of the page.
- `confluence`
  - `id`: `pageId` from Confluence.
  - `space`: `spaceKey` from Confluence.
  - `markup`: `html`, `markdown`, `wiki` (default: `wiki`)

The `confluence.markup` value determines is HTML or Confluence wiki markup is used for the conversion. Each seems to have strengths and weaknesses. We don't use markdown at all because Confluence does markdown poorly. Maybe one day all this can be avoided.

## Usage

In your CI/CD Settings, set the following:

- `CONFLUENCE_API`: API base URL. e.g. https://confluence.foo.io/rest/api
- `CONFLUENCE_USERNAME`
- `CONFLUENCE_PASSWORD`: This can be a token.

Import this template:

``` yaml
include:
  - remote: 'https://gitlab.com/theherk/markdown-to-confluence-gitlab-ci-template/-/raw/main/.markdown-to-confluence.yml'
```

Then, just ensure you have `build` and `deploy` stages. Those are part of the [default stages](https://docs.gitlab.com/ee/ci/yaml/#stages) if none are given, but if you have other stages defined, you'll need to add them explicitly.

``` yaml
stages:
  - ...
  - build
  - deploy
```

That's it. Now, any push events with modified markdown files will trigger this workflow.

## Images Used

- [bitnami/git](https://hub.docker.com/r/bitnami/git/)
- [mikefarah/yq](https://github.com/mikefarah/yq)
- [bash](https://hub.docker.com/_/bash/)
- [linuxserver/yq](https://github.com/linuxserver/docker-yq)
  - This is not for yq, but rather sed and jq in the same image.
- [pandoc/core](https://hub.docker.com/r/pandoc/core)
  - For conversion from markdown to Confluence / Jira wiki markup or HTML.
- [curlimages/curl](https://github.com/curl/curl-docker)
